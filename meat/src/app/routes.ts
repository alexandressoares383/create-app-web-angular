import { Routes } from '@angular/router';

/* Components */
import { HomeComponent } from './components/home';
import { AboutComponent } from './components/about';
import { RestaurantsComponent } from './components/restaurants';

export const ROUTES: Routes = [
  { path: '', component: HomeComponent},
  { path: 'about', component: AboutComponent},
  { path: 'restaurants', component: RestaurantsComponent}
]
